import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaginatorComponent } from './components/paginator/paginator.component';
import { NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { PaginatorRoutingModule } from './paginator-routing.module';

@NgModule({
  imports: [
    CommonModule,
    PaginatorRoutingModule,
    NgbPaginationModule
  ],
  exports: [
  PaginatorComponent
],
  declarations: [PaginatorComponent]
})
export class PaginatorModule { }
