import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Page } from 'src/app/shared/model/page';

@Component({
  selector: 'app-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.css']
})
export class  PaginatorComponent<T> implements OnInit {
  @Input() paginator: Page<T>;
  @Output() pageChanged = new EventEmitter<number>();

  pageNumber: number;
  numberOfElements: number;
  pageSize: number;
  maxSize: number = 8;
  rotate: boolean = true;
  ellipses: boolean = false;
  boundaryLinks: boolean = true;

  constructor() { }

  ngOnInit() {
    this.pageNumber = this.paginator.number + 1;
    this.pageSize = this.paginator.size;
    this.numberOfElements = this.paginator.totalElements;
  }

  pageChange(page: number): void {
    this.pageChanged.emit(page - 1);
  }

}
