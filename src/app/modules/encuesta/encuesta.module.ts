import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormularioComponent } from './components/formulario/formulario.component';
import { ListaEncuestaComponent } from './components/lista-encuesta/lista-encuesta.component';
import { EncuestaRoutingModule } from './encuesta-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { PaginatorModule } from '../paginator/paginator.module';
@NgModule({
  declarations: [FormularioComponent, ListaEncuestaComponent],
  imports: [
PaginatorModule,
    CommonModule,
    HttpClientModule,
    EncuestaRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class EncuestaModule { }
