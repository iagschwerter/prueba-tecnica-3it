import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Encuesta } from '../models/encuesta';
import { Page } from 'src/app/shared/model/page';
import { Response } from '../models/response';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class EncuestaService {
  url: string;

  constructor(private http: HttpClient) {
  this.url=environment.url+"v1/encuesta";
  }



    getAll(size: number, page: number): Observable<Page<Encuesta>> {

        console.log(page);

      let path: string = this.url+"/getall" + "?size=" + size + "&page=" + page;

      return this.http.get<Page<Encuesta>>(path);
    }

  graba(encuesta:Encuesta){
        return this.http.post<Response>(this.url+"/crear",encuesta);
  }

}
