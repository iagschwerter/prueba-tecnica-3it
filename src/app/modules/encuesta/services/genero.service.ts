import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Genero } from '../models/genero';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GeneroService {
  url: string;

  constructor(private http: HttpClient) {
  this.url=environment.url+"v1/genero/getall";
  }


  getAll(){
        return this.http.get<Genero[]>(this.url);
  }


}
