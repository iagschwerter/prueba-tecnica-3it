import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Genero } from '../../models/genero';
import { Encuesta } from '../../models/encuesta';
import { GeneroService } from '../../services/genero.service';
import { EncuestaService } from '../../services/encuesta.service';
import { AlertService } from 'src/app/modules/alert/services/alert.service';
@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {
  editor: FormGroup;
  generos:Genero[];
  encuestaSubmit: Encuesta = new Encuesta();
  constructor(private generoService:GeneroService, private encuestaService:EncuestaService,
    private alertService: AlertService) { }

    ngOnInit() {
      this.generoService.getAll().subscribe(data => {
        this.generos=data;
      });
      this.editor = this.getEncuestaEditor();
    }

    submit(){


      if(this.editor.controls['email'].value==""){
        this.alertService.error("Email Debe Ser Ingresado", false);
        return;
      }
      if(!this.esEmailValido(this.editor.controls['email'].value)){
        this.alertService.error("Email no es Valido", false);
        return;
      }
      if(this.editor.controls['genero'].value=="0"){
        this.alertService.error("Debe Seleccionar un Genero", false);
        return;
      }
      this.encuestaSubmit.email=this.editor.controls['email'].value;
      this.encuestaSubmit.genero=this.editor.controls['genero'].value;

     this.encuestaService.graba(this.encuestaSubmit).subscribe(data => {
        console.log(data);
        if(data.status=="ERROR"){
            this.alertService.error(data.message, false);
        }else{
            this.alertService.success("Encuesta Guardado Correctamente", false);
            this.ngOnInit();
        }

      });

    }

    esEmailValido(email: string):boolean {
      let mailValido = false;

      var EMAIL_REGEX = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

      if (email.match(EMAIL_REGEX)){
        mailValido = true;
      }
      return mailValido;
    }
    getEncuestaEditor(): FormGroup {
      return new FormGroup({
        email: new FormControl('', [
          Validators.required,
          Validators.min(1)
        ]),
        genero: new FormControl('0', [
          Validators.required
        ]),
      });
    }

  }
