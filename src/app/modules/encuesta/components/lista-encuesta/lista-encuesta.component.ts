import { Component, OnInit } from '@angular/core';
import { Encuesta } from '../../models/encuesta';
import { Response } from '../../models/response';
import { EncuestaService } from '../../services/encuesta.service';
import { Page } from 'src/app/shared/model/page';


@Component({
  selector: 'app-lista-encuesta',
  templateUrl: './lista-encuesta.component.html',
  styleUrls: ['./lista-encuesta.component.css']
})
export class ListaEncuestaComponent implements OnInit {
encuestas: Encuesta[];
    page: Page<Encuesta>;
            defaultSize: number;
            loading:boolean;
  constructor( private encuestaService:EncuestaService) { }

  ngOnInit() {
    this.loading=true;
            this.defaultSize = 7;
            this.getEncuesta(0);
  }
cambiosize(event){

  this.defaultSize=event.target.value;
    this.getEncuesta(0);
}
  getEncuesta(page: number): void {
    this.encuestaService.getAll(this.defaultSize, page).subscribe(data => {
            this.page = data;
      this.encuestas=this.page.content;
      console.log(data);

    this.loading=false;
    })
  }
}
