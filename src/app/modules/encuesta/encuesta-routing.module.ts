import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FormularioComponent } from './components/formulario/formulario.component';
import { ListaEncuestaComponent } from './components/lista-encuesta/lista-encuesta.component';

const routes: Routes = [
  { path: 'formulario', component: FormularioComponent },
    { path: 'lista', component: ListaEncuestaComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EncuestaRoutingModule { }
